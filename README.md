# RobSuite Dev Project.

## VCV Rack Plugin - RobSuite

### List Of Modules
1. VCO-R1
1. VCA-R1
1. EmptyWig
1. Mutes-R1
1. Lead-R1
1. ADSR-R1
1. Replicator-R1
1. Unity-R1

## Synth Modules

### Lead-R1 Details
* Lead-R1 is a full Synth module, that only requires midi-input and somehting to accept the signal output.
* It is comprised of a VCO, VCA, and ADSR.
* Choose between Triangle or Saw waves.
* High or Low Octave selector. 
* Full ADSR control with 4 individual knobs.
* Master Volume Knob.
* 2 Inputs - CV (V/OCT) and Gate Inputs.
* 1 Output - Complete sound. Send it directly to the output audio if you want.
* Inbuilt Ladder Filter with High and Low pass.
* 3 Controll Knobs for the filter: Filter Frequency, Resonance, and Drive.

## Tool Modules

### OscTool-R1 Details
* OscTool-R1 is a tool module that is primarily designed to combine up to 10 different oscilators into one output to create multi voiced Sounds.
* 10 seperate Oscilator channels with individual Level and Pan controlls.
* Connect the gate of any 1 ADSR and controll all the Oscilators.
* Individual Left, Right and Mono Outputs.
* Master volume Controll. 
* V/Oct input to controll the pitch of all oscilators from another module (Example: Midi-1 module)
* Controll the Merging of the oscilators to either add the voltages or Average them.
* 'TO' OUTPUT is the V/oct output to connect to an oscilator.
* 'FROM' INOUT is the output voltage of the oscilator.



### Replicator-R1
* Takes 1 input, and replicates it out of 10 outputs.
* chain them together to get Increased replication.

